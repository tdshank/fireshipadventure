# Firebase, NextJS and Dark Magic 

### Note: 

- To get a more robust version, view branches. All versions come with a minimal working example of Jest, NextJS and Firebase for setup.

- Default -> `main/clone` 
    - Firebase
    - NextJS
    - Jest
    
## Technical Overview:

### Scenario:

Inspired by social media platforms, similar to devto and medium.

This style of app present a wide variety of challenges

## ***Challenges***: 
---
## NextJS 12

### Dynamic Content

- Content that is search engine friendly.
    - Dynamic
    - Interactive

Goal: Limit the total number of reads executed on the firestore by implementing best practices.

We can write code that behaves nicely with the search engines, giving us SEO Super Powers with Server Side (NextJS) rendering.

- Pagination! (caveat: Data will not listen to realtime updates in the firestore.)
    - On other pages, we will transition from server rendered, to fully realtime data from firestore.

- Full Server Sided Rendering

- Dynamic Routing

- ISR (Incremental Static Regeneration)

- File Upload
---
## Jest v27.2

- Working unit testing with Jest 

---
## Firebase SDK v9.3

### Emulators

- Writing Jest Tests using Firebase Emulators 

- Setting up Firebase test environment using connectXService to emulatorX strategy 

- Save a lot of personal Firebase resources and making Firebase happy
### Security

- Working Firebase rules 

- Firebase Rules Unit Testing
Jest + @firebase/rules-unit-testing
```
//jest - https://www.npmjs.com/package/jest
//rules-unit-testing - https://www.npmjs.com/package/@firebase/rules-unit-testing
```
---