import {FC} from 'react';

/**
 * Functions related to the login page should stay in the login page.
 *
 * This programming practice will reduce the javascript loaded potentially?
 */
import {onAuthStateChanged, signInWithPopup} from 'firebase/auth';
import Image from 'next/image';


import {auth, gApiPro,} from '../../lib/firebase/init';
import i from '../../public/images/g-sign-in.png';

/**
 *
 * @returns
 */
export const GSignInButton: FC = () => {
    let signInWithGoog;

    try {
        signInWithGoog = async () => {
            await signInWithPopup(auth, gApiPro);
            onAuthStateChanged(auth, () => {
                console.log("Auth updated!")
            })
        };
    } catch (e) {
        console.error(e);
    }

    return (
        <button className="btn-google" onClick={signInWithGoog}>
            <Image width={60} height={60} alt="Image failed to load" src={i}/>{" "}
            Sign In With Google
        </button>
    );
};
