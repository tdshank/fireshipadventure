import { FC } from 'react';

import { auth } from '../../lib/firebase/init';

// Sign out button
export const SignOutButton: FC = () => {
	return <button onClick={() => auth.signOut()}>Sign Out</button>;
};
