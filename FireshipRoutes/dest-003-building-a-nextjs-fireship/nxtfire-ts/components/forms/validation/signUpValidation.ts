import * as Yup from 'yup';

export default Yup.object().shape({
	username: Yup.string()

		.min(1, "Username must be at least 1 character.")
		.max(36, "Username must be less than 36 characters.")
		.required("Required"!),

	// email: Yup.string()
	//   .email("Please supply a valid email")
	//   .required("Required!"),
	// password: Yup.string()
	//   .min(8, "Please supply a password")
	//   .max(120, "Too many characters!")
	//   .matches(
	//     /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/,
	//     "Must Contain, \n One Upper (A), \n  One Lower (a), \n  One Number (7) \n One Special (@)"
	//   )
	//   .required("Required!"),
	// repeatPassword: Yup.string().oneOf(
	//   [Yup.ref("password"), null],
	//   "Passwords must match"
	// ),
	// key: Yup.string(),
	// secret: Yup.string(),
	// partAComplete: Yup.boolean(),
});
