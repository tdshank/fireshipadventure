import {FC, useContext, useEffect, useState,} from 'react';

import {writeBatch,} from 'firebase/firestore';
import {FormikHelpers, useFormik,} from 'formik';

import {User} from '@firebase/auth';
import {TextField} from '@mui/material';

import {firestore} from '../../lib/firebase/init';
import {useMui, UserContext,} from '../../lib/react';
import {useCheckUsername} from '../../lib/react/hooks/util/useCheckUsername';
// VALIDATION SCHEMA
import validationSchema from './validation/signUpValidation';
import {userDocument} from "../../lib/firebase/models";
import {usernameDocument} from "../../lib/firebase/models/username";
//_________________________________________________________________________________________
/**
 * User Form Types
 */

export type SignupFormState = {
    username?: string;
};

//_________________________________________________________________________________________
/**
 * User Form Helpers
 */

const initialValues: SignupFormState = {
    username: "",
};

// Handle Form Submission
const onSubmit = (user: User) => (
    values: SignupFormState,
) =>
    new Promise(async () => {
        console.log(`USER UID`, user.uid)
        console.log(`USER FORM VALUES`, values);

        // HANDLE ERRORS
        if (!user) throw new Error("User Undefined in Session");
        if (!values.username) throw new Error("No Username Supplied!");

        // Get a reference to each document for batch writing.
        const userDoc = userDocument(user.uid);
        const usernameDoc = usernameDocument(values.username);

        // Perform Batch operation.
        await writeBatch(firestore)
            .set(userDoc, {
                uid: user.uid,
                username: values.username,
                photoURL: user.photoURL,
                displayName: user.displayName,
            })
            .set(usernameDoc, {uid: user.uid, username: values.username})
            .commit();
    })
//_________________________________________________________________________________________
/**
 * FORM
 */

/**
 * ## Scenario:
 *
 *    Only allowing unique usernames amongst users, but allowing them
 * to user anonymous usernames upon login on choice.
 *
 * onSubmit is not called everytime the form is rendered, we can nest
 * our firebase data fetching logic in here to keep it from re-fetching
 * on every render.
 */

export const UserForm: FC = () => {
    // Environment Hooks
    // const {mui, setTheme} = useMui();
    const {user} = useContext(UserContext);

    // User Form State Hooks
    const [loading, setLoading] = useState(true);
    const [helperText, setHelperText] = useState("username");

    // Formik Hook
    const formik = useFormik({
        initialValues,
        validationSchema,
        onSubmit: onSubmit(user!), // Override the formik.handleSubmit function.
    });

    // Validate Username Hook
    const validateUsername = useCheckUsername({
        ...formik,
        setHelperText,
        setLoading,
    });

    // React useEffect Hook
    let username = formik.values.username!;
    useEffect(() => {
        validateUsername(username);
    }, [username, validateUsername]);

    // Render Form
    return (
        <section>
            <h3>Choose Username</h3>
            <form
                onSubmit={(e) => {
                    e.preventDefault();
                    formik.handleSubmit(e);
                }}
            >
                <TextField
                    id="username"
                    name="username"
                    label="username"
                    value={formik.values.username}
                    onChange={(e) => {
                        setLoading(true);
                        formik.setFieldTouched("username", true);
                        formik.handleChange(e);
                    }}
                    error={
                        formik.touched.username &&
                        Boolean(formik.errors.username)
                    }
                    helperText={
                        formik.touched.username && formik.errors.username
                            ? formik.errors.username
                            : helperText
                    }
                />
                <br/>
                <button
                    type="submit"
                    className="btn-green"
                    disabled={
                        !!formik.errors.username ||
                        !formik.isValid ||
                        formik.isSubmitting ||
                        formik.isValidating
                    }
                >
                    Choose
                </button>

                <h3> Debug State </h3>
                <div>
                    Username: {formik.values.username}
                    <br/>
                    Loading: {loading.toString()}
                    <br/>
                    Username Valid: {(!formik.errors.username).toString()}
                </div>
            </form>
        </section>
    );
};
