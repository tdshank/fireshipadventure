import { Button, Snackbar } from "@mui/material";
import { FormikState, FormikComputedProps } from "formik";
import { ReactNode, FC, useState } from "react";

type AlertSnackProps<T = {}> = {
	msg: string;
	state?: FormikState<T> & Partial<FormikComputedProps<T>>;
	fragment?: ReactNode;
	debug?: boolean;
};

export const AlertSnack: FC<AlertSnackProps> = ({
	msg,
	fragment,
	state,
	debug,
	children,
}) => {
	const [open, setOpen] = useState(false);

	const handleClick = () => {
		setOpen(true);
	};

	const handleClose = (event: any, reason: any) => {
		console.log(`Alert Toast Close Reason:`, reason);
		if (reason === "clickaway") {
			return;
		}

		setOpen(false);
	};

	return (
		<>
			{debug ? (
				<Button onClick={handleClick}> Open Toast Alert Test </Button>
			) : undefined}
			<Snackbar
				draggable={true}
				open={open}
				autoHideDuration={6000}
				onClose={handleClose}
				message={
					msg
						? msg
						: `Error has no message. isValid:${
								state ? state.isValid : false
						  }`
				}
				action={
					fragment ? (
						fragment
					) : children ? (
						children
					) : (
						<>Default Fragment</>
					)
				}
			/>
		</>
	);
};
