import {Dispatch, FC, MouseEventHandler, SetStateAction, useContext, useEffect, useState} from 'react';

import Link from 'next/link';
import {Post, readUserPostsGroup} from "../../lib/firebase/models";
import {limit, orderBy, startAfter, where} from "firebase/firestore";
import Loader from "./Loader";
import {UserContext} from "../../lib/react";
import $ from 'jquery'
export const PAGINATE = 3;

type GetMorePostProps = {
    setLoading: Dispatch<SetStateAction<boolean>>,
    setPosts: Dispatch<SetStateAction<Post[]>>
    setPostsEnd: Dispatch<SetStateAction<boolean>>
    postsState: Post[]
}
const getMorePosts = async ({postsState, setPosts, setLoading, setPostsEnd}: GetMorePostProps) => {
    setLoading(true)

    const {out: paginatedPosts} = await readUserPostsGroup({
        constraints: [
            where("published", "==", true),
            orderBy("createdAt", "desc"),
            startAfter(postsState[postsState.length - 1].createdAt),
            limit(PAGINATE)
        ]
    })

    setPosts(postsState.concat(paginatedPosts!))
    setLoading(false)

    if (paginatedPosts!.length < PAGINATE)
        setPostsEnd(true)
}

const PostItem: FC<{ post: Post; admin: boolean }> = ({
                                                          post,
                                                          admin,
                                                      }) => {
    const wordCount = post.content?.trim().split(/\s+/g).length + 1;
    const minutesToRead = (wordCount! / 100 + 1).toFixed(0);

    return (
        <div className="card">
            <Link passHref={true} href={`/${post.displayName}`}>
                <a>
                    <strong>By @{post.displayName}</strong>
                </a>
            </Link>

            <Link passHref={true} href={`/${post.displayName}/${post.slug}`}>
                <h2>
                    <a>{post.title}</a>
                </h2>
            </Link>

            <footer>
				<span>
					{wordCount} words. {minutesToRead} min read <br/>
					Published: {new Date(Number(post.createdAt)).toString().split("GMT")[0]}
				</span>
                <span className="push-left">
					🍻 {post.likeCount || 0} cheers
				</span>
            </footer>


            {/* If admin view, show extra controls for user */}
            {admin && (
                <>
                    <Link passHref={true} href={`/admin/${post.slug}`}>
                        <h3>
                            <button className="btn-blue">Edit</button>
                        </h3>
                    </Link>

                    {post.published ? (
                        <p className="text-success">Live</p>
                    ) : (
                        <p className="text-danger">Unpublished</p>
                    )}
                </>
            )}
        </div>
    );
};

const PostFeed: FC<{ posts: Post[] }> = ({
                                                    posts,
                                                }) => {
    const {user, username} = useContext(UserContext)
    const [postsState, setPosts] = useState(posts)
    const [postsEnd, setPostsEnd] = useState(false)
    const [loading, setLoading] = useState(false)
    const [postFeedHeight, setPostFeedHeight] = useState(0)

    const handleClick: MouseEventHandler<HTMLButtonElement> = async (e) => {
        e.stopPropagation()
        e.nativeEvent.stopImmediatePropagation()
        setPostFeedHeight($(".index-ul").height()!+100)
        await getMorePosts({
            postsState,
            setPosts,
            setPostsEnd,
            setLoading
        })
        $(document).scrollTop($(document).height()!)
    }

    console.log(`Post Feed Height ${postFeedHeight}`)
    return postsState ?
        loading ? <Loader style={{marginBottom: postFeedHeight}} show={loading}/> :
            (
                <div className="index-ul-container">
                    <ul className="index-ul">
                        {postsState.map((post) => (
                            <PostItem key={post.slug} post={post} admin={false}/>
                        ))}
                    </ul>

                    {user && username ?
                        postsEnd ? <div>At the end of posts!</div> :
                            <button onClick={handleClick}>Get more posts</button>
                        : null}


                </div>
            ) : null;
}

export default PostFeed

