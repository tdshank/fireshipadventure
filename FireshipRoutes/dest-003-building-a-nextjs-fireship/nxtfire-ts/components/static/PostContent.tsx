import Link from 'next/link'
import ReactMarkdown from 'react-markdown'
import {FC} from "react";
import {Post} from "../../lib/firebase/models";
import {Timestamp} from "@firebase/firestore-types";

type PostContentProps = {
    post: Post
}
const PostContent: FC<PostContentProps> = ({post}) => {
    const createdAtDate: Date = typeof post.createdAt === 'number' ? new Date(post.createdAt) : (post.createdAt as Timestamp).toDate()

    return (
        <div className={"card"}>
            <h1>{post?.title}</h1>
            <span className="text-sm">
                Written by{' '}
                <Link href={`/${post.username}/`}>
                <a className="text-info">@{post.username}</a>
                    </Link>{' '}
                    on {createdAtDate.toISOString()}
                </span>
            <ReactMarkdown>{post?.content}</ReactMarkdown>
        </div>
    )
}
export default PostContent
