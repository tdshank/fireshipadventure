import {CSSProperties, FC} from "react";

type LoaderProps = {
	show: boolean
	style: CSSProperties
}

export const Loader: FC<LoaderProps> = ({ show, style }) => {
	return show ? <div style={style} className="loader"></div> : null;
};

export default Loader
