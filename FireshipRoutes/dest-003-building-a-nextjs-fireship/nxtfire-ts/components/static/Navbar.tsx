import {FC, useContext} from 'react';

import {signOut} from 'firebase/auth';
import Image from 'next/image';
import Link from 'next/link';

import {auth} from '../../lib/firebase/init';
import {AlertSnack} from '../popups/AlertSnack';
import troll from '../../public/images/unauth-user.png'
import {UserContext} from "../../lib/react";

export const NavBar: FC = () => {
    // GET USER DATA FROM FIRESTORE HOOK
    const {user, username} = useContext(UserContext)

    return (
        <nav className="navbar">
            <ul>
                <li>
                    <Link passHref href="/">
                        <button className="btn-logo">{user ? "FEED" : "NXT"}</button>
                    </Link>
                </li>

                {/* user is signed-in and has a username */}

                {username && user && (
                    <>
                        <li className="push-left">
                            <button onClick={() => signOut(auth)}>
                                Sign Out
                            </button>
                        </li>
                        <li>
                            <Link passHref href="/admin">
                                <button
                                    onClick={() => (
                                        <AlertSnack msg="Alert Message"/>
                                    )}
                                >
                                    admin
                                </button>
                            </Link>
                        </li>
                        <li>
                            <Link passHref href={`/${username}`}>
                                {/*This wrapper div is required as we are not allowed to pass href from LINK FC to any other*/}
                                {/*predefined FC. It must be passed on to a plain html element.*/}
                                <div>
                                    <Image
                                        width={60}
                                        height={60}
                                        alt="Image failed to load!"
                                        src={user.photoURL || troll}
                                    />
                                </div>
                            </Link>
                        </li>
                    </>
                )}

                {/* user does not have a username, and is not signed. */}
                {!username && (
                    <>
                        <li>
                            <Link passHref href="/enter">
                                <button className="btn-blue">Log in</button>
                            </Link>
                        </li>
                    </>
                )}
            </ul>
        </nav>
    );
};
