import { FC } from "react";

import { User } from "../../lib/firebase/models";
import troll from '../../public/images/unauth-user.png'
import Image from 'next/image'
export const UserProfile: FC<{ user: User }> = ({ user }) => {

	return (
		<div className="box-center">
			{/*	alt="Image filed to load"*/}
			{/*	className="card-img-center"*/}

			<Image
				alt="Image not found"
				className="user-profile-image"
				src={user.photoURL || troll}
				layout="fixed"
				width={150}
				height={150}
				//sizes={"(max-width: 1023px) 80px, 80px"}
			/>
			<p>
				<i>@{user.username}</i>
			</p>
			<h1>{user.displayName || "Anon User"}</h1>
		</div>
	);
};
