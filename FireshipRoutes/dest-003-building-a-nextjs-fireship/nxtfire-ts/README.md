This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Doesn't Run?

Check that `babel.config.js` is `not in` the `src/ directory`. This will cause NextJS to look at that configuration instead of their default since NextJS uses their own. Move it to `tests/` and try again.

## Getting Started

First, run the development server:

```bash
npm install
# and
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.page.ts`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

# Comments

## Git Notes
- `Deleting a branch` - git branch -d [name]
- `Deleting a remote branch` - git push origin --delete [name]
    - (It is a good idea to run both of these commands to make sure local/remote are syncd)

## Extra Tech


- `MaterialUI V5`
- `Formik`
- `Cypress E2E Testing`
- `Firestore Rules Testing`

## Future Tech To Consider

- `Apollo Client / Server Solution for Graphql Request`

## Running Firestore Unit Tests

### *Important*
`babel.config.js must be moved from tests/ to the same dir as package.json` (src/ typically), 

## Running Cypress End to End Tests

Not Configured

