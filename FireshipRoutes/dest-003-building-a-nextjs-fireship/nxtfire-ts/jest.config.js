module.exports = {
	//preset: "ts-jest",
	testPathIgnorePatterns: ["cypress"],
	testMatch: ["**/__tests__/**/?(*.)+(test).[jt]s?(x)"],
	// //transform: { "^.+\\.(ts|tsx)$": "ts-jest" },
	maxWorkers: 1,
};
