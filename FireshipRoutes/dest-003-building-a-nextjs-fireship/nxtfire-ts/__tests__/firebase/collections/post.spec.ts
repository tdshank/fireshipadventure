/**
 * Post Tests
 */
import {
    converter,
    getPostCollection,
    getPostDocumentRefById,
    getUserPostDocument,
    Post,
    postCollection,
    postDoc,
    readAllPosts,
    readAllUsersPosts,
    setPostCol,
    setPostDoc,
    user,
    writePostCollectionDocument
} from "../../../lib/firebase/models";
import {assertSucceeds} from "@firebase/rules-unit-testing";
import {collection} from "firebase/firestore";
import {firestore} from "../../../lib/firebase/init";

/**
 * @function postTests
 *
 * @requires user
 *  - Requires a user object to be instantiated for Private Requests
 *  @see lib/models/firebase/user.ts
 *  @see __tests__/util/helpers.ts
 */
export const postTests = () => {
    // Get Post Document Ref
    it(`[public]: Get Post Collection Reference`, async () => {
        expect(setPostCol(await getPostCollection(user.uid))).toBeDefined()
        expect(postCollection).toHaveProperty("id")
    })

    //Write Post Data to a Post Collection Reference using UID [users/{uid}/posts].
    it(`[public|private]: Write post to Post Collection`, async () => {
        setPostDoc((await writePostCollectionDocument({
            user,
            ref: postCollection,
            data: {
                uid: user.uid,
                displayName: user.displayName,
                title: "Jest Test Post",
                content: "Lorem Bipsum",
                slug: "jest-test-post",
                likeCount: 0,
                published: false,
                createdAt: new Date(),
                updatedAt: new Date(),
            }
        })).ref)
    })

    it(`[public]: Get User Post Document Reference (By Username, Auth and Uid)`, () => {
        expect(getPostDocumentRefById({
            user,
            ref: postDoc,
        })).toHaveProperty("id")
    })

    it(`[public] Read User Post`, async () =>
        assertSucceeds(getUserPostDocument({
            user,
            ref: postDoc,
        }))
    )

    it(`[public] Real All Users Posts`, async () =>
        assertSucceeds(readAllUsersPosts({
            user,
            ref: postCollection
        }))
    )

    it(`[public] Read all Posts in Store`, () =>
        assertSucceeds(readAllPosts({
            user,
            ref: collection(firestore, "posts").withConverter(converter<Post>())
        }))
    )
    // it(`[Public|Private]: Write Post Document {users/{uid}/posts}`, () =>
    // 	assertSucceeds(
    // 		writePostCollectionDocument({
    // 			user: testUser,
    // 			data: {
    // 				uid: testUser.uid,
    // 				displayName: testUser.displayName!,
    // 				title: "Jest Test Post",
    // 				content: "Lorem Bipsum",
    // 				slug: "jest-test-post",
    // 				likeCount: 0,
    // 				published: false,
    // 				createdAt: new Date(),
    // 				updatedAt: new Date(),
    // 			},
    // 		}),
    // 	));
    //
    // // Get Post where uid == post.uid
    // it(`[Public|Private]: Get Post Collection Query {users/{uid}/posts/{ where path.uid == post.uid }}`, () =>
    // 	assertSucceeds(
    // 		getUserPostCollection({
    // 			user: testUser,
    // 		}),
    // 	));
    //
    // // Get Post document by direct uid `/posts/{uid}`
    // it(`[Private]: Get Users Post Document`, () =>
    // 	assertSucceeds(
    // 		getUserPostDocument({
    // 			user: testUser,
    // 		}),
    // 	));
};
