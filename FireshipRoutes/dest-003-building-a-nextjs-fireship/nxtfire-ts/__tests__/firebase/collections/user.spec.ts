import {useAuth,} from '../../util/helpers';
import {
    queryUserCollection,
    setUserDoc, User,
    user,
    userCollection,
    UserDoc,
    userDoc,
    userDocument,
    writeUserDocument
} from "../../../lib/firebase/models";
import {assertFails, assertSucceeds} from "@firebase/rules-unit-testing";
import {FirestoreAction} from "../../../lib/types";

/**
 * -- Initial Setup
 * @see [__tests__\util\firebaseSetup.ts]
 * @see [__tests__\firebase\firebase.test.ts]
 */
export const userTests = () => {
    /**
     * With the new functions, what do we need to do?
     * 1 - Get a userDoc ref and set it to ./helpers.ts global
     *
     *
     * */

    /**
     * Env Setup Tests
     */
    it(`[ENV]: Initial Data`, async () => {
        //CHECK USER INIT DATA
        //console.log(`Test user defined on setup`, user)
        expect(user).toBeDefined();
    }, 1000);
    // Get a User Doc Reference
    it(`[public]: Get User Document Reference`, async () => {
        expect(setUserDoc(userDocument(user.uid))).toHaveProperty("id")
        expect(userDoc).toBeDefined()
    }, 1000)

    // Write a User Doc to Firestore
    it(`[private|public]: Write User Document`, async () =>
            assertSucceeds(writeUserDocument({
                user, // Uses data from user object if
                ref: userDoc
            }))
        , 1000)

    it(`[private] Query users Collection`, async () => {
        const args: FirestoreAction<User, "q", "ref"> = {
            user,
            ref: userCollection()
        }
        return useAuth ?
            assertSucceeds(queryUserCollection(args)) :
            assertFails(queryUserCollection(args))
    }, 5000)

    /**
     * User Tests
     */
    // it(`[Private]: Get User by Username`, () =>
    // 	assertSucceeds(queryUserByUsername({ user: testUser, data: {username: testUser.username!} })));
};
