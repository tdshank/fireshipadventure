import {firebaseSetup} from '../util/firebaseSetup';
import {teardownFirestore} from '../util/teardown';
import {postTests, userTests,} from './collections';

// Jest Test Branch Init
/**
 * // ----- SETUP ----- \\
 */
beforeAll(async () => {
    //Override setupVars Here
    await firebaseSetup({
        useAuthentication: true,
    });
}, 20000);

describe("_Firestore Rules Unit Tests_", () => {
    /**
     * [Firestore User] rules unit tests
     */
    describe("[User]", userTests);
    describe("[Post]", postTests);
});

/**
 * // ----- TEARDOWN ----- \\
 */
afterAll(async () => {
    await teardownFirestore();
});
