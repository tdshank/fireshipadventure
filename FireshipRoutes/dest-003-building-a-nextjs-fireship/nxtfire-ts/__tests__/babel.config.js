// MOVE TO SRC/ WHEN RUNNING TESTS
module.exports = {
	presets: [
		["@babel/preset-env", { targets: { node: "current" } }],
		"@babel/preset-typescript",
	],
};
