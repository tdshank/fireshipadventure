# Jest Testing with Firestore

## Gotchas | Things I wish I knew

### *Errors: [`Test suite must contain at least one test` ]*

? Error will appear with a describe. 
- Nowhere on the internet could I find a solution to get jest to shut up about
 .test files running without a full describe/it/expect structure. 
- With this jest.config, .spec files will be ignored and we still get the jest environment

```javascript
// jest.config.json
module.exports = {
	testPathIgnorePatterns: ["cypress"],
	testMatch: ["**/__tests__/**/?(*.)+(test).[jt]s?(x)"],
	//transform: { "^.+\\.(ts|tsx)$": "ts-jest" },
	maxWorkers: 1,
};

// usersTest.spec.ts will not longer complain
export const userTests = () => {
	it(`Write User ${username} [Authenticated:${usingAuth}]`, async () => {
		await assertSucceeds(
			writeUserDoc(...),
		);
	});

	it(`Get user ${username} [Authenticated:${usingAuth}]`, async () => {
		await assertSucceeds(getUserByUid(...));
	});
};
```
