import faker from 'faker';
import {signInAnonymously} from 'firebase/auth';
import {v4 as uuid} from 'uuid';

import {RulesTestEnvironment, TestEnvironmentConfig,} from '@firebase/rules-unit-testing';
import {assertionError} from '@firebase/util';

import {auth} from '../../lib/firebase/init';
import {merge} from "lodash";
import {setUser, User} from "../../lib/firebase/models";
//______________________________________________________________________________________________________________________
/**
 * Setup Variables
 */
// Default UID [FOR JEST TEST]
export let uid: string = "";
// Username [FOR JEST TESTS]
export let username = "Kryptum";
// Use Auth?
export let useAuth: boolean = true;
/**
 * [Jest Firebase testEnv reference]
 */
export let fireTestEnv: RulesTestEnvironment;
//______________________________________________________________________________________________________________________
/**
 * Helper Methods
 * */
export const refOrUserNull = (ref: any, user?: Partial<User>,) => {
    if (!user || !ref) throw !user ? USERNULL() : DOCNULL()
}
export const DOCNULL = () => error(new Error("PERMISSION_DENIED Doc Data undefined"));
export const USERNULL = () => error(new Error("PERMISSION_DENIED User is null"))
export const error = (e: Error) => e.message.includes("PERMISSION_DENIED") ? assertionError(e.message) : e;

/**
 * Sets helper variable useAuth
 * @param it {@link useAuth}
 * @returns it
 */
export const setUseAuth = (it: boolean) => (useAuth = it);
export const setTestEnv = (it: RulesTestEnvironment) => {
    fireTestEnv = it;
};

/**
 * generates a uuid v4 compatible with Firestore paths,
 * @param prefix - For testing What works in a Firestore path.
 * @returns prefix.length > 0 ? { prefix +"_"+ uid } : { uid }
 *
 */
export const getFirestoreCompatUid = (prefix: string = "") => {
    let count = 0;

    uuid()
        .split("_")
        .forEach((hunk) => {
            count == 0 && prefix.length > 0 && uid.concat(prefix + "-");

            uid += hunk;
            count = count + 1;
        });
    return uid;
};


/**
 * Wraps e in assertionError for Jest Unit testing.
 * @param e any (Error)
 * @returns assertionError for [assertSucceeds | assertFails] for firebase/util
 */

/**
 * signInAnon test environment: if {@link useAuth} == true
 * @param initData
 * @returns helpers/useAuth ? authenticated user : unauthenticated user
 */
export const signInAnon = async (initData: User) =>
    setUser(merge(
            setUserDefaults(initData),
            (useAuth ? (await signInAnonymously(auth)).user : {})
        )
    )

/**
 * objVal: UserDoc
 * srcVal: User
 * */

/**
 * Merges testUser with a defaulted object and returns result
 *    Prepares doc data for writing
 * @returns default user data
 */
export const setUserDefaults = (
    initData: Partial<User> = {},
) => ({
    uid: initData.uid ?? getFirestoreCompatUid(), // Should never be null
    username: initData.username ?? username ?? fakename(),
    displayName: initData.displayName ?? fakename(),
    photoURL: initData.photoURL ?? "public/images/unauth-user.png",
});

/*
 * Emulator Service Configurations
 */
export const firestoreEmulatorConfig: TestEnvironmentConfig["firestore"] = {
    host: "localhost",
    port: 8080,
};

export const storeEmulatorConfig: TestEnvironmentConfig["storage"] = {
    host: "localhost",
    port: 9199,
};

export const fakename = () =>
    faker.name.firstName() + " " + faker.name.lastName();


/**
 * Generates a random INT number by between min and max
 * @param min
 * @param max
 * @returns number randomized between min and max
 */
export const between = (min: number, max: number) =>
    Math.floor(Math.random() * (max - min) + min)