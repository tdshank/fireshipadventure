import {auth} from '../../lib/firebase/init';
import {fireTestEnv} from './helpers';

declare module "firebase/auth" {
}

/**
 * - if auth:
 *  - Delete User
 *    - Sign out
 * - Clear Firestore
 * - Cleanup Firebase
 * - Clear all Jest Mocks
 */
export const teardownFirestore = async () => {
    console.info("[Teardown: Tearing Down Firebase Test Environment]");
    if (auth.currentUser) {
        console.info("[Teardown: Removing Anonymous user from emulator]");
        await auth.currentUser.delete();
        await auth.signOut();
    }
    await fireTestEnv.clearFirestore();
    await fireTestEnv.cleanup();
    jest.clearAllMocks();
};
