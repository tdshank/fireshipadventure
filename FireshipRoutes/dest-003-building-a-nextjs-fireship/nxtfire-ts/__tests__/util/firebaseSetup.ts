/**
 * Firebase Test Setup
 */

import faker from 'faker';
import {connectAuthEmulator} from 'firebase/auth';
import {connectFirestoreEmulator} from 'firebase/firestore';

import {initializeTestEnvironment} from '@firebase/rules-unit-testing';

import {auth, firestore, projectId,} from '../../lib/firebase/init';
import {
    between,
    firestoreEmulatorConfig,
    setTestEnv,
    setUseAuth,
    signInAnon,
    storeEmulatorConfig,
} from './helpers';
import {MAX_SAFE_NUMBER, MIN_SAFE_NUMBER} from "../../lib/util/statics";
import {User, UserDoc} from "../../lib/firebase/models";
//______________________________________________________________________________________________________________________
/**
 * Firebase Test Related Types
 */

type SetupEnvironment = {
    useAuthentication: boolean;
    userData: Partial<UserDoc>;
};
//______________________________________________________________________________________________________________________
/**
 * Firebase Setup Methods
 * */

/**
 * Setup firebase test environment
 * @param vars = {
 * - userData: {@link UserDoc}
 * - useAuthentication: {@link boolean}
 * }
 */
export const firebaseSetup = async ({
                                        userData,
                                        useAuthentication,
                                    }: Partial<SetupEnvironment>) => {
    try {
        // Seed Faker
        faker.seed(between(MIN_SAFE_NUMBER, MAX_SAFE_NUMBER));

        // Connect Emulators
        if(process.env['NODE_ENV'] ==="development" && !auth.emulatorConfig){
            connectFirestoreEmulator(firestore, "localhost", 8080);
            connectAuthEmulator(auth, "http://localhost:9099", {
                disableWarnings: true,
            });
        }

        // Set Static Helpers
        setUseAuth(useAuthentication ?? false);

        // Sign in to Environment @see helpers.ts#useAuth
        await signInAnon(userData as User)
    } catch (e) {
    }
};
