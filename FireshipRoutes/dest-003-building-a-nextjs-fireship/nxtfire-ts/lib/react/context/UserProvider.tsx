import {createContext, FC,} from 'react';

import {useUserData} from '../hooks';

// Represents the structure of our noSql document.
type AuthContext = {
    // props
    user: any;
    username: any;
};

/**
 * Recognize that these represent the field structure
 * specified in our firestore in console.
 */
const contextDefault: AuthContext = {
    user: null,
    username: null,
};

export const UserContext = createContext<AuthContext>(contextDefault);

export const UserProvider: FC = ({children}) => {
    return (
        <UserContext.Provider value={useUserData()}>
            {children}
        </UserContext.Provider>
    );
};