import { useEffect, useState } from "react";
import { Theme, useTheme } from "@mui/material/styles";

// Do Custom things with the theme here
export const useMui = () => {
	const theme = useTheme();
	const [mui, setTheme] = useState<Theme | null>(theme);

	useEffect(() => {
		mui ? setTheme(mui) : setTheme(null);
	}, [mui]);

	return { mui, setTheme };
};
