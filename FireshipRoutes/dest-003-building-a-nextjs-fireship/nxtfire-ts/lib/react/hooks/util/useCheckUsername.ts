import {Dispatch, SetStateAction, useCallback,} from 'react';

import {FormikErrors} from 'formik';
import {debounce} from 'lodash';

import {getDoc} from '@firebase/firestore';

import {SignupFormState} from '../../../../components/forms/UserForm';
import {userDocument} from '../../../firebase/models';
import {usernameDoc, usernameDocument} from "../../../firebase/models/username";

type CheckUsernameVars = {
    setHelperText: Dispatch<SetStateAction<string>>;
    setLoading: Dispatch<SetStateAction<boolean>>;
    setErrors(errors: FormikErrors<SignupFormState>): void;
};

/**
 * @function useCheckUsername
 * Users lodash.debounce to determine when a user stops typing
 * to check if their entered input is valid.
 *  - useCallback(debounce(checkName))
 *
 * @param CheckUsernameVars React Dispatch Functions
 */
export const useCheckUsername = ({
                                     setHelperText,
                                     setLoading,
                                     setErrors
                                 }: CheckUsernameVars) =>
    useCallback(
        debounce(async (val: string) => {
            if (val.length >= 1 && val.length <= 36) {
                // Get Snapshot of Username From Database
                const ref = usernameDocument(val);
                const snap = await getDoc(ref);
                // Check if username exist,

                snap.exists()
                    ? // Username Exists
                    setErrors({
                        username: "That username is not available!",
                    })
                    : // Username Does not Exist
                    setHelperText("Username available!");

                setLoading(false);
            }
        }, 800),
        [],
    );
