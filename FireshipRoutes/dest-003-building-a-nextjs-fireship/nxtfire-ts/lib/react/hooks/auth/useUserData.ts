import {useEffect, useState,} from 'react';

import {doc, onSnapshot,} from 'firebase/firestore';
import {useAuthState} from 'react-firebase-hooks/auth';

import {auth, firestore,} from '../../../firebase/init';

/**
 * @function userUserData
 * Sets the initial user Auth object before they register.
 *    @see {@link components/buttons/GSignInButton}
 *    @see {@link components/forms/UserForm}
 */
export const useUserData = () => {
    const [user] = useAuthState(auth);
    const [username, setUsername] = useState(null);

    useEffect(() => {
        if (user) {
            return onSnapshot(doc(firestore, "users", user.uid), (d) => {
                return setUsername(d.data()?.username);
            });
        }// This is where our users Auth object supplies the initial User Document.

        else return setUsername(null)
    }, [user]);

    return {user, username};
};