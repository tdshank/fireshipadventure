/**
 * FIREBASE POST MODEL
 */
import {
    addDoc,
    collection,
    collectionGroup,
    CollectionReference,
    doc, DocumentData,
    DocumentReference,
    getDoc,
    getDocs,
    query,
    QuerySnapshot,
    Timestamp,
    writeBatch
} from 'firebase/firestore';

import {DOCNULL, fakename, refOrUserNull,} from '../../../__tests__/util/helpers';
import {FirestoreAction, FirestoreResponse} from '../../types';
import {User} from "./user";
import {firestore} from "../init";
import {MutableKeys} from "utility-types";
import {forEach} from "lodash";
import {makeDateSerializable} from "../../util/serialization";
import {v4 as uuid} from "uuid";
import {converter} from "./index";

//______________________________________________________________________________________________________________________
/**
 * POST RELATED TYPES
 * */
interface IPost {
    uid: string,
    username: string,
    displayName: string,
    title: string,
    content: string,
    slug: string,
    likeCount: number,
    published: boolean,
    createdAt: Timestamp | Date | number,
    updatedAt: Timestamp | number,
}


export type Post = IPost
export type PostDocument = DocumentReference<IPost>
export type PostCollection = CollectionReference<IPost>

export type PostDocumentKeys = MutableKeys<Post>
export const postDocumentKeys = [
    "uid",
    "displayName",
    "title",
    "content",
    "slug",
    "likeCount",
    "published",
    "createdAt",
    "updatedAt",
] as const
//______________________________________________________________________________________________________________________
/**
 * HELPERS
 * */
// Default UID [FOR JEST TEST]
export let postId: string = "";
// Username [FOR JEST TESTS]
export let postSlug = "";
/**
 * [Authenticated user reference]
 */
export let post: Post;
export let posts: Post[]
/**
 * [userDoc reference]
 */
export let postDoc: DocumentReference<Post>;
export let postCollection: PostCollection


/**
 * sets testUser environment variable {@link user}
 * @param it Test user to set
 * @returns it
 */
export const setPost = (it: Partial<Post>) => (post = it as Post);
export const setPostDoc = (it: PostDocument) =>
    (postDoc = it);
export const setPostCol = (it: PostCollection) =>
    (postCollection = it)
//______________________________________________________________________________________________________________________
/**
 * GET REFERENCE METHODS
 * */


export const getPostCollection = (uid: string) =>
    collection(firestore, `users/${uid}/posts`).withConverter(
        converter<Post>(),
    )

export const getPostDocumentByPath = (path: string) =>
    doc(firestore, path).withConverter(converter<Post>())


export const getPostDocumentRefById = ({ref, user}: FirestoreAction<Partial<Post>, "doc", "ref">) => {
    refOrUserNull(ref, user)
    return doc(firestore, `users/${user!.uid}/posts/${ref!.id}`).withConverter(
        converter<Post>(),
    );
}

export const getPostDocumentRefBySlug = ({user, data}: FirestoreAction<Partial<Post>, "doc", "ref">) =>
    doc(firestore, `users/${user!.uid}/posts/${data?.slug}`).withConverter(converter<Post>())


/**
 * ```
 *  collectionGroup(firestore, "posts").withConverter(
 *      converter<Post>()
 *   )
 * ```
 * */
export const getPostCollectionGroup = () =>
    collectionGroup(firestore, "posts").withConverter(
        converter<Post>()
    )

//______________________________________________________________________________________________________________________
/*
 * FIRESTORE ACTIONS
 * */

/**
 * - `users/{uid}/posts`
 * - Writes a document reference to memory
 * @param createPostData
 */
export const writePostCollectionDocument = ({
                                                user,
                                                data,
                                                ref
                                            }: FirestoreAction<Post, "col", "ref">) =>
    new Promise<FirestoreResponse<Post, "doc", "ref">>(async resolve => {
        refOrUserNull(ref, user)
        if (!data) throw DOCNULL()

        // Get Post Document Reference.
        const oRef = await addDoc(ref!, data! as Post);
        if (!oRef || !oRef.id) throw DOCNULL()

        resolve({ref: oRef});
    });

export const writeFakePosts = (amount: number, {user}: FirestoreAction<Post, "col", "ref">) =>
    new Promise<FirestoreResponse<Post, "q", "snap">>(async (resolve, reject) => {
        if (!user) return reject(DOCNULL())
        const batch = writeBatch(firestore)

        while (amount--) {
            const tempSlug = uuid()
            batch.set(doc(firestore, `users/${user.uid!}/posts/${tempSlug}`), createFakePost(Timestamp.now(), user, tempSlug, amount))
        }

        await batch.commit()
    })

/**
 * - users/{uid}/posts
 *  A Collection reference is not necessary for this method
 * `await getDocs(query(
 *          getPostCollectionWithUid(user!.uid!),
 *  ).withConverter(converter<Post>()))
 * @param getPostData
 */
export const readAllUsersPosts = ({user}: FirestoreAction<Post, "col", "ref">) =>
    new Promise<FirestoreResponse<Post, "q", "snap", []>>(async (resolve, reject) => {
        if (!user) return reject(DOCNULL())
        let snap: QuerySnapshot<Post>
        let out: Post[] = []

        try {
            snap = await getDocs(query(
                getPostCollection(user!.uid!),
            ).withConverter(converter<Post>()));
        } catch (e) {
            console.log(`There was an error fetching the posts!`, e)
            return reject(e)
        }

        if (snap.size === 0) throw DOCNULL()

        snap.docs.forEach(doc => out.push(doc.data()))
        if (!out) throw DOCNULL();

        return resolve({snap, out});
    });

export const createFakePost = (timestamp: Timestamp, user?: User, slug?: string, postNum = 0): Post => ({
    slug: slug ?? uuid(),
    username: user?.username ?? fakename(),
    content: "null",
    uid: user?.uid! ?? "null",
    title: `Generated Post ${postNum}`,
    displayName: user?.username! ?? fakename(),
    likeCount: 0,
    published: true,
    createdAt: timestamp.toMillis() - (postNum * 1000),
    updatedAt: timestamp.toMillis() - (postNum * 1000)
})

export const readUserPosts = ({user, constraints}: FirestoreAction<Post, "q", "ref">) =>
    new Promise<FirestoreResponse<Post, "q", "snap", []>>(async (resolve, reject) => {
        console.log(`Trying to read`, user, "posts")
        let posts: Post[] = []
        const snap = await getDocs(query(
            getPostCollection(user!.uid!), ...constraints!
        ))

        if (snap.empty) return resolve({
            snap: snap,
            out: postsToJSON([createFakePost(Timestamp.now())])
        })

        forEach(snap.docs, doc => posts.push(doc.data()))

        resolve({
            out: postsToJSON(posts),
            snap: snap
        })
    })

export const readUserPostsGroup = ({user, constraints}: FirestoreAction<Post, "q", "ref">) =>
    new Promise<FirestoreResponse<Post, "q", "snap", []>>(async resolve => {

        let posts: Post[] = []
        const snap = await getDocs(query(
            getPostCollectionGroup(), ...constraints!
        ))

        if (snap.empty) {
            console.log(`UserGroup posts array returned empty, generating an array`)
            return resolve({
                snap: snap,
                out: []
            })
        }

        snap.docs.forEach(doc => posts.push(doc.data()))

        resolve({
            out: postsToJSON(posts),
            snap: snap
        })

    })


export const readAllPosts = ({user, ref}: FirestoreAction<Post, "col", "ref">) =>
    new Promise<FirestoreResponse<Post, "q", "snap", []>>(async (resolve, reject) => {
        refOrUserNull(ref, user)
        let snap: QuerySnapshot<IPost>
        let out: Post[] = []
        try {
            snap = await getDocs(query(ref!))
        } catch (e) {
            return reject(e)
        }
        if (snap.size === 0) throw DOCNULL()
        snap.docs.forEach(doc => out.push(doc.data()))

        resolve({
            snap,
            out
        })
    })

/**
 * Takes a ref
 * @param user
 * @param ref
 */
export const getUserPostDocument = ({user, ref}: FirestoreAction<Post, "doc", "ref">) =>
    new Promise<FirestoreResponse<Post, "doc", "snap">>(async (resolve, reject) => {
        console.log(`User and Ref,`,  user, ref)
        refOrUserNull(ref, user)

        const snap = await getDoc(ref!);
        console.log(`Post exists?`, snap.exists(), snap.data())
        if (!snap.exists()) return reject(DOCNULL())

        console.log(`Post document`, snap.data())
        resolve({
            out: postToJSON(snap.data()),
            snap
        })
    });

/**
 * SERIALIZATION
 */
/**
 * @param posts Snap of Posts from firebase to Serialize
 * @returns document data as PostDocFields
 */
const postsToJSON = (posts: Post[]): Post[] => posts.map(postToJSON);

const postToJSON = (qDocSnap: DocumentData) => {

    if(!qDocSnap)
        return qDocSnap

    return {
        ...qDocSnap,
        createdAt: makeDateSerializable(qDocSnap.createdAt),
        updatedAt: makeDateSerializable(qDocSnap.updatedAt)
    } as Post;
};

// /**
//  * Full Function
//  *
//  * Gets { FirestoreRes\<PostDocument\> } from
//  * - ` users/{uid}/posts/{ where path.uid === post.uid } `
//  * @param getPostData
//  */
// export const getUserPostCollection = ({ user }: FirestoreAction<PostDocument>) =>
// 	new Promise<FirestoreRes<PostDocument>>(async (resolve, reject) => {
// 		if (!user) return reject(DOCNULL());
//
// 		const colRef = getPostCollectionWithUid(user!.uid);
// 		const q = query(colRef).withConverter(converter<PostDocument>());
// 		const qSnap = await getDocs(q);
// 		const out = qSnap.size == 1 ? qSnap.docs[0].data() : null;
// 		if (!out) return reject(DOCNULL());
//
// 		return resolve({
// 			q,
// 			qSnap,
// 			out,
// 		});
// 	});


//______________________________________________________________________________________________________________________
// EXAMPLE OF COLLECTIONGROUP
// export const getUsersPostCollectionGroup = async (num: number) =>
// 	getDocs(
// 		query(
// 			getUserCollectionGroup("posts"),
// 			where("published", "==", true),
// 			orderBy("createdAt", "desc"),
// 			limit(num),
// 		),
// 	);
