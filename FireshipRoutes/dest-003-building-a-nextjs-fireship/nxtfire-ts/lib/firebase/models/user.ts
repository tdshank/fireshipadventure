/**
 * USER SNAPS
 */

import {
    collection,
    doc,
    DocumentReference,
    getDoc,
    getDocs,
    query,
    QueryConstraint,
    QuerySnapshot,
    setDoc,
    where,
} from 'firebase/firestore';

import {DOCNULL, refOrUserNull,} from '../../../__tests__/util/helpers';
import {FirestoreAction, FirestoreResponse} from "../../types";
import {User as U} from "@firebase/auth";
import {MutableKeys} from "utility-types";
import {firestore} from "../init";
import {getUserPostDocument} from "./post";
import {converter} from "./index";

//______________________________________________________________________________________________________________________
/**
 * USER RELATED TYPES
 * */
interface IUser {
    uid: string | null;
    displayName: string | null;
    photoURL: string | null;
    username: string | null;
}

export type User = IUser;
export type UserDoc = DocumentReference<User>
export type UserDocKeys = MutableKeys<IUser>;
export const userDocKeys = [
    "uid",
    "displayName",
    "photoURL",
    "username",
] as const;
//______________________________________________________________________________________________________________________
/**
 * HELPERS
 * */
/**
 * [Authenticated user reference]
 */
export let user: User;
/**
 * [userDoc reference]
 */
export let userDoc: UserDoc;
/**
 * sets testUser environment variable {@link user}
 * @param it Test user to set
 * @returns it
 */
export const setUser = (it: Partial<User>) => (user = it as User);
export const setUserDoc = (it: DocumentReference<User>) =>
    (userDoc = it);
//______________________________________________________________________________________________________________________

/**
 * GET REFERENCE METHODS
 * */
export const userDocument = (...pathSegments: string[]) =>
    doc(firestore, "users", ...pathSegments).withConverter(
        converter<User>(),
    );

export const userCollection = () =>
    collection(firestore, "users").withConverter(
        converter<User>(),
    );

export const userQuery = (...queryConstraints: QueryConstraint[]) =>
    query(userCollection(), ...queryConstraints)
//______________________________________________________________________________________________________________________
/**
 * FIRESTORE ACTIONS
 * */

/**
 * Gets { FirestoreRes\<UserDoc\> } from
 * - `user/{ where request.auth.uid === user.uid }`
 *
 * @param writePostData
 * @throws AssertionError
 */
export const readUserDocument = ({user, ref}: FirestoreAction<User, "doc", "ref">) =>
    new Promise<FirestoreResponse<User, "doc", "snap">>(async resolve => {
        refOrUserNull(ref, user)

        const snap = await getDoc(ref!);
        if (!snap.exists()) throw DOCNULL()

        const out = snap.data();
        if (!out) throw DOCNULL();

        resolve({out, snap});
    });

/**
 * Writes { FirestoreSet<UserDoc> } to
 * - `users/{uid}`
 * @param writePostData
 * @throws AssertionError
 */
export const writeUserDocument = ({
                                      user,
                                      data,
                                      ref
                                  }: FirestoreAction<User, "doc", "ref">) =>
    new Promise<boolean>(async (resolve) => {
        refOrUserNull(ref, user)

        await setDoc(
            ref ?? doc(firestore, `users/${user!.uid}`),
            data ?? getUserDocFromUser(user!))

        return resolve(true)
    });

export const getUserDocFromUser = (user: User): Partial<User> => ({
    uid: user.uid,
    username: user.username!,
    displayName: user.displayName!,
    photoURL: user.photoURL!
})

export const queryUserCollection = async ({ref, user}: FirestoreAction<User, "q", "ref">) =>
    new Promise<FirestoreResponse<User, "q", "snap">>(async (resolve, reject) => {
        refOrUserNull(ref, user)
        let snap: QuerySnapshot<User>

        try {
            snap = await getDocs(ref!)
        } catch (e) {
            return reject(e)
        }

        if (snap!.size == 0) throw DOCNULL()

        resolve({
            snap: snap!

        })
    })

/**
 * Get User By UID
 * Does not require ref object.
 */
export const readUserByUid = async ({user}: FirestoreAction<User, "doc", "ref">) =>
    new Promise<FirestoreResponse<User, "doc", "snap">>(async resolve => {
        if(!user) throw DOCNULL()

        const snap = await getDoc(userDocument(user!.uid!))
        if(!snap.exists()) throw DOCNULL()

        resolve({
            snap,
            out: snap.data()
        })
    })


/**
 * Get User by their Username
 * @requires `user` to be defined
 */
export const queryUserByUsername = async ({data, user}: FirestoreAction<User, "col", "ref">) =>
    new Promise<FirestoreResponse<User, "q", "snap">>(async (resolve, reject) => {
        let username = user?.username || data?.username!;

        const snap = await getDocs(
            query(userCollection()!, where("username", "==", username))
        )

        if (snap.size == 0) {
            console.log(`Username Query returned snapsize == 0 {REJECTED}`)
            return reject(DOCNULL())
        }

        return resolve({
            out:  snap.docs[0].data(),
            snap
        })
    })


