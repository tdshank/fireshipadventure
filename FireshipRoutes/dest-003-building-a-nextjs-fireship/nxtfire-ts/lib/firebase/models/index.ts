// index exports
import {DocumentData, FirestoreDataConverter, WithFieldValue} from "firebase/firestore";

// Doc Converter
export const converter = <T>(): FirestoreDataConverter<T> => ({
    fromFirestore: (snap: DocumentData) => snap.data(),
    toFirestore: (o: DocumentData) => o,
});

export * from "./post";
export * from "./user";