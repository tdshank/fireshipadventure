/**
 * USER SNAPS
 */

import {
    collection,
    doc,
    DocumentReference,
    getDoc,
    getDocs,
    query,
    QueryConstraint,
    QuerySnapshot,
    setDoc,
    where,
} from 'firebase/firestore';

import {DOCNULL, refOrUserNull,} from '../../../__tests__/util/helpers';
import {FirestoreAction, FirestoreResponse} from "../../types";
import {User as U} from "@firebase/auth";
import {MutableKeys} from "utility-types";
import {firestore} from "../init";
import {getUserPostDocument} from "./post";
import {converter} from "./index";

//______________________________________________________________________________________________________________________
/**
 * USER RELATED TYPES
 * */
interface IUsername {
    uid: string | null;
    username: string
}

export type Username = IUsername;
export type UsernameDoc = DocumentReference<Username>
export type UserDocKeys = MutableKeys<IUsername>;
export const userDocKeys = [
    "uid",
    "displayName",
    "photoURL",
    "username",
] as const;
//______________________________________________________________________________________________________________________
/**
 * HELPERS
 * */
/**
 * [Authenticated user reference]
 */
export let username: Username;
/**
 * [userDoc reference]
 */
export let usernameDoc: UsernameDoc;
/**
 * sets testUser environment variable {@link username}
 * @param it Test user to set
 * @returns it
 */
export const setUser = (it: Partial<Username>) => (username = it as Username);
export const setUserDoc = (it: DocumentReference<Username>) =>
    (usernameDoc = it);
//______________________________________________________________________________________________________________________

/**
 * GET REFERENCE METHODS
 * */
export const usernameDocument = (...pathSegments: string[]) =>
    doc(firestore, "usernames", ...pathSegments).withConverter(
        converter<Username>(),
    );

export const usernameCollection = () =>
    collection(firestore, "usernames").withConverter(
        converter<Username>(),
    );

export const usernameQuery = (...queryConstraints: QueryConstraint[]) =>
    query(usernameCollection(), ...queryConstraints)
//______________________________________________________________________________________________________________________
/**
 * FIRESTORE ACTIONS
 * */
