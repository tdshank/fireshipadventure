// INITIALIZE FIREBASE
import {FirebaseOptions, getApp, getApps, initializeApp,} from 'firebase/app';
import {Auth, connectAuthEmulator, getAuth, GoogleAuthProvider,} from 'firebase/auth';
import {
    connectFirestoreEmulator,
    doc,
    DocumentData,
    Firestore,
    FirestoreDataConverter,
    getFirestore,
    Timestamp,
} from 'firebase/firestore';

/**
 * TYPE HELPERS
 */
export type FirebaseCollectionLiterals =
    | "usernames"
    | "users"
    | "posts"
    | "likes";
export type FirebaseTypes = "c" | "d";
/**
 * FIREBASE APP
 */

// App Id

export const projectId = "njs-fs";

// Config

const firebaseOpt: FirebaseOptions = {
    projectId,
    apiKey: "AIzaSyB_WgGapWhoVQnnaVYF6tsNopauVrqxtZI",
    authDomain: "njs-fs.firebaseapp.com",
    storageBucket: "njs-fs.appspot.com",
    messagingSenderId: "180663164600",
    appId: "1:180663164600:web:b1c856fc5d6212199f9e4d",
    measurementId: "G-714915PQ6Z",
};

// App

if (!getApps().length) {
    initializeApp(firebaseOpt, projectId)
};
export const firestore: Firestore = getFirestore(getApp(projectId));
export const auth: Auth = getAuth(getApp(projectId));
export const gApiPro = new GoogleAuthProvider();



// Emulators (DEV)
(async () => {
    if (process.env['NODE_ENV'] === "development" && !auth.emulatorConfig) {
        connectFirestoreEmulator(firestore, "localhost", 8080)
        connectAuthEmulator(auth, "http://localhost:9099", {
            disableWarnings: true,
        })
    }
})();


/**
 * HELPER FUNCTIONS
 */



export const fromMillis = Timestamp.fromMillis;
