import {
    CollectionReference,
    DocumentReference,
    DocumentSnapshot,
    Query,
    QueryConstraint,
    QuerySnapshot,
} from 'firebase/firestore';
import {Post, User} from "./firebase/models";

/**
 * MODULE AUGMENTATIONS
 * */
declare module 'firebase/auth' {
    interface User {
        username: string
    }
}

// Firebase Unit Testing Context

export type FirestoreRefLiterals = "doc" | "col" | "q"
export type GetFirestoreRefType<TModel, LRef extends FirestoreRefLiterals> =
    LRef extends "doc" ? DocumentReference<TModel> :
        LRef extends "col" ? CollectionReference<TModel> :
            Query<TModel>

export type GetFirestoreSnapType<TModel, LRef extends FirestoreRefLiterals> =
    LRef extends "doc" ? DocumentSnapshot<TModel> : QuerySnapshot<TModel>

export type FirestoreResLiterals = "ref" | "snap"


interface IFirestoreAction<TModel,
    LRef extends FirestoreRefLiterals,
    LRes extends FirestoreResLiterals> {
    user?: User | undefined
    ref?: GetFirestoreRefType<TModel, LRef>
}

export type GetFirestoreResType<TModel,
    LRef extends FirestoreRefLiterals,
    LRes extends FirestoreResLiterals> =
    LRes extends "ref" ?
        GetFirestoreRefType<TModel, LRef> :
        GetFirestoreSnapType<TModel, LRef>

interface IFirestoreResponse<TModel,
    LRef extends FirestoreRefLiterals,
    ResType extends FirestoreResLiterals, Wrap extends any> {
    out?: Wrap extends Array<TModel> ? Array<TModel> : TModel | undefined
}


export type FirestoreAction<TModel,
    LRef extends FirestoreRefLiterals,
    LRes extends FirestoreResLiterals> = IFirestoreAction<TModel, LRef, LRes>
    & FirestoreActionData<TModel, LRef, LRes>

export type FirestoreActionData<TModel,
    LRef extends FirestoreRefLiterals,
    LRes extends FirestoreResLiterals,
    QueryOrSeg = SegmentsOrConstraints<LRef>> =
    QueryOrSeg & { data?: Partial<TModel> }

type SegmentsOrConstraints<LRef extends FirestoreRefLiterals> =
    LRef extends "q" ? { constraints?: QueryConstraint[] } : { pathSegments?: string[] }

type FirestoreSnapOrRef<TModel, LRef extends FirestoreRefLiterals, LRes extends FirestoreResLiterals> =
    LRes extends "ref" ?
        { ref: GetFirestoreRefType<TModel, LRef> } :
        { snap: GetFirestoreSnapType<TModel, LRef> }

export type FirestoreResponse<TModel,
    LRef extends FirestoreRefLiterals,
    LRes extends FirestoreResLiterals,
    Wrap extends any = undefined> =
    IFirestoreResponse<TModel, LRef, LRes, Wrap>
    & FirestoreSnapOrRef<TModel, LRef, LRes>

