import { FormikState, FormikComputedProps } from "formik";

export const handleFormikFieldError = <T,>(
	key: keyof T,
	state: FormikState<T> & Partial<FormikComputedProps<T>>,
) => {
	const falsey = fieldFalsey(key, state);

	if (falsey) {
		const error = state.errors[key];
		const msg = error?.toString();
		console.error(`ERROR HANDLER =>`, error);

		const fragment = <>Alert Toast Fragment</>;

		//TODO Remove Button (DEBUGGING)
		return <></>;
	}

	return falsey;
};

export const fieldFalsey = <T,>(
	key: keyof T,
	state: FormikState<T> & Partial<FormikComputedProps<T>>,
) => state.touched[key] && (state.errors[key] || state.isValid);
