import {Timestamp} from 'firebase/firestore'


/**
 * SERIALIZATION HELPERS
 */

// SERIALIZE POSTS



export const makeDateSerializable = (val: Date | Timestamp) => {
    console.log(`handling date`, val, (val instanceof Timestamp))
    return val instanceof Timestamp ? val.toMillis() : val
}
