import {GetServerSideProps, NextPage} from 'next';
import {UserContext} from "../lib/react";
import Metatags from "../components/Metatags";
import {UserForm} from "../components/forms";
import {GSignInButton} from "../components/buttons";
import {useContext, useEffect} from "react";
import router from "next/router";


export const getServerSideProps: GetServerSideProps = async (context) => {

    try {

        return {
            props: context.query
        }
    } catch (e) {
        return {
            notFound: true
        }
    }
}

//
const Enter: NextPage = () => {
    const {user, username} = useContext(UserContext)

    useEffect(() => {
        if (user && username)
            router.push(`/${username}`)
    }, [user, username])

    return (
        <main>
            <Metatags title="Enter" description="Sign up for this amazing app!"/>
            {user && !username ? <UserForm/> : <GSignInButton/>}
        </main>
    );
};

/**
 * DEFAULT PAGE EXPORT (REQUIRED)
 */
export default Enter;