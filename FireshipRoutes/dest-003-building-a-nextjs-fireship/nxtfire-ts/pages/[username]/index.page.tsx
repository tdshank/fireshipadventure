/**
 * NOTES,
 * - This route will be utilized when the user types anything in the route besides an existing page.
 * - Does this conflict with existing admin and enter routes?
 *  - No, Because NextJS prioritizes to any routes that exist statically versus dynamically.
 * */

/**
 * POSTS PAGE FOR [USERNAME]
 */

import {GetServerSideProps, NextPage} from 'next';
import {UserProfile} from "../../components/static/UserProfile";
import {Post, queryUserByUsername, readUserPosts, User, writeFakePosts} from '../../lib/firebase/models'
import {limit, orderBy} from "firebase/firestore";
import PostFeed from "../../components/static/PostFeed";

type UserProfilePageProps = {
    user: User
    posts: Array<Post>
}
//
export const getServerSideProps: GetServerSideProps<UserProfilePageProps> = async (
    context
) => {
    try {
        const {username} = context.query
        const {out: user} = await queryUserByUsername({
            data: {
                username: username as string,
            }
        })

        if (!user)
            return {
                notFound: true
            }

        const {out: posts} = await readUserPosts({
            user,
            constraints: [
                orderBy("createdAt", "desc"),
                limit(5)
            ]
        })


        return {
            props: {
                user: user!,
                posts: posts!
            }
        }
    } catch (e) {

        // if ((e as Error).message.includes("PERMISSION_DENIED Doc Data undefined")) {
        //     console.log(`User by the username of ${context.query.username} does not exist in Firestore`)
        //     return {
        //         redirect: {
        //             permanent: false,
        //             destination: "/",
        //
        //         }
        //     }
        // }

        console.error(e)
        return {
            notFound: true
        }
    }
}

const UserProfilePage: NextPage<UserProfilePageProps> = ({user, posts}) => {

    return (
        <main>
            <button onClick={async () => {
                await writeFakePosts(10, {user: user!})
            }}>Create more posts
            </button>
            <UserProfile user={user}/>
            <PostFeed posts={posts}/>
        </main>
    );
};

export default UserProfilePage;