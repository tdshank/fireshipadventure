import {GetStaticPaths, GetStaticProps, NextPage} from "next";
import {
    converter, getPostCollectionGroup,
    getPostDocumentByPath,
    getPostDocumentRefBySlug, getUserPostDocument,
    Post as PostType,
    queryUserByUsername,
    User
} from "../../lib/firebase/models";
import {collectionGroup, getDoc, getDocs, limit, onSnapshot, orderBy, query} from "firebase/firestore";
import {firestore} from "../../lib/firebase/init";
import {useRouter} from "next/router";
import {useEffect, useState} from "react";
import {Loader} from "../../components/static";
import PostContent from "../../components/static/PostContent";


//Executes first?
export const getStaticPaths: GetStaticPaths = async () => {
    console.log("Trying to get static paths...")
    const snap = await getDocs(
        query(
            getPostCollectionGroup(),
            orderBy("createdAt", "desc"),
            limit(5)
        )
    )

    const paths = snap.docs.map((doc) => {
        const {slug, username} = doc.data()
        return {
            params: {username, slug}
        }
    })

    console.log(`Paths located`, paths[0], "...")

    return {
        paths,
        fallback: true,
    }
}

export const getStaticProps: GetStaticProps = async (context) => {

    const {username, slug} = context.params!
    console.log(`Props`, username, slug)

    const {out: user} = await queryUserByUsername({
        data: {
            username: username as string
        }
    })
    console.log(`Located user,`, user)

    const postRef = getPostDocumentRefBySlug({
        user,
        data: {
            slug: slug as string
        }
    })

    const path = postRef.path

    const {out: post} = await getUserPostDocument({
        user,
        ref: postRef
    })

    return {
        props: {
            user,
            post,
            path
        },
        revalidate: 5000
    }
}

export const Post: NextPage<{ post: PostType; user: User, path: string }> = (context) => {
    let post: PostType
    const [postState, setPostState] = useState(context.post)
    const router = useRouter()

    useEffect(() => {
        if(context.path && context.path.length > 0)
            return onSnapshot(getPostDocumentByPath(context.path), snap => {
                setPostState(snap.data()!)
            })
    }, [setPostState, context])

    post = postState || context.post

    if (router.isFallback) {
        return <main><Loader show/></main>
    }

    return (
        <main>

            <section>
                <PostContent post={post}/>
            </section>

            <aside className="card">
                <p>
                    <strong>{post.likeCount || 0} 🍻</strong>
                </p>

            </aside>


        </main>
    );
};


export default Post
