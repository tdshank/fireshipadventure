import {GetServerSideProps, NextPage} from 'next';
import Link from 'next/link'
import {Post, readUserPostsGroup} from "../lib/firebase/models";
import {limit, orderBy, where} from "firebase/firestore";
import {useContext} from "react";
import PostFeed, {PAGINATE} from "../components/static/PostFeed";
import {UserContext} from "../lib/react";

type IndexProps = {
    posts: Post[]
}

export const getServerSideProps: GetServerSideProps<IndexProps> = async (context) => {
    try {

        const {out: posts} = await readUserPostsGroup({
            constraints: [
                where("published", "==", true),
                orderBy("createdAt", "desc"),
                limit(PAGINATE)
            ]
        })

        return {
            props: {
                posts: posts!
            }
        }
    } catch (e) {
        return {
            notFound: true
        }
    }
}


const Home: NextPage<IndexProps> = (props) => {
    const {user, username} = useContext(UserContext)

    return (
        <main>

            <div className="card card-info">
                <h2> NextJS, Firebase and Material UI. </h2>
                <p> Welcome to the art of dark magics {username ?? null}</p>
                <p> {!user ? " Sign up for an account and get started!" :
                    !username ? "Please Register a username" : null} </p>

                {/*Example of an Explicit Link*/}
                {(!user || !username) ? <Link href={{
                    pathname: '/enter'
                }}> Go </Link> : null}

            </div>

            {
                props.posts.length > 0 &&
                <PostFeed posts={props.posts}/>
            }


        </main>
    );
};

/**
 * DEFAULT PAGE EXPORT (REQUIRED)
 */
export default Home;