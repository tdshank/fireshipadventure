import '../styles/globals.css'

import {AppProps} from 'next/dist/shared/lib/router/router'
import {NavBar} from "../components/static";
import {UserProvider} from "../lib/react";

const MyApp = ({Component, pageProps}: AppProps) => {
    return (
        <UserProvider>
            <NavBar/>
            <Component {...pageProps}/>
        </UserProvider>
    )
};

export default MyApp