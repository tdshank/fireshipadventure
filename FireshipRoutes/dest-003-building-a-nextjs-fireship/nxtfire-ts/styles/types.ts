import { ColorPartial } from "@mui/material/styles/createPalette";

/**
 * ---------------------------------------------------------
 * MUI OVERRIDES START
 */
type FK = "nav";

type CustomThemeFields = {
	main?: string;
};

declare module "@mui/material/styles" {
	// interface Theme {
	// 	status: {
	// 		danger: string;
	// 	};
	// }
	// // allow configuration using `createTheme`
	// interface ThemeOptions {
	// 	status?: {
	// 		danger?: string;
	// 	};
	// }

	interface PaletteOptions {
		// Break Recursion
		nav: Omit<PaletteOptions, FK> & CustomThemeFields;
	}
}

// OVERRIDE DEFAULT MUI INTERFACES (Shows the sub colors defined in primary/secondary)
declare module "@mui/material/styles/createPalette" {
	interface PaletteColor extends ColorPartial {}
}

/**
 * MUI OVERRIDES END
 * ---------------------------------------------------------
 */
