import { createTheme } from "@mui/material/styles";

/**
 * ---------------------------------------------------------
 * DECLARE MAIN THEME
 */

export const mainTheme = createTheme({
	palette: {
		mode: "dark",
		nav: {
			//TODO MIGHT BE A THEMING CONFLICTION
			primary: {
				main: "#1E2529",
				"100": "#6930C3",
				"200": "#5E60CE",
				"300": "#5390D9",
				"400": "#4EA8DE",
				"500": "#48BFE3",
				"600": "#56CFE1",
				"700": "#64DFDF",
				"800": "#72EFDD",
				"900": "#80FFDB",
			},
			secondary: {
				main: "#151B25",
				"50": "#1A202C",
				"100": "#192E41",
				"200": "#52688F",
				"300": "#BBC8DE",
			},
			text: {
				primary: "#FFF",
				secondary: "#000000",
			},
			background: {
				default: "#321575",
				paper: "#08070a",
			},
		},
	},
	typography: {
		body2: {
			color: "white",
		},
		allVariants: {
			color: "white",
		},
	},
});

/**
 * MAIN THEME END
 * ---------------------------------------------------------
 */
